/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MDT_Digitizer.h"
#include "../MDT_Response_DigiTool.h"
#include "../MdtDigitizationTool.h"
#include "../RT_Relation_DB_DigiTool.h"

DECLARE_COMPONENT(MDT_Digitizer)
DECLARE_COMPONENT(MDT_Response_DigiTool)
DECLARE_COMPONENT(RT_Relation_DB_DigiTool)
DECLARE_COMPONENT(MdtDigitizationTool)
